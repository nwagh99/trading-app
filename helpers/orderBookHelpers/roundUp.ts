export default function round(num: number, decimal = 10000): number {
  return Math.round(num * decimal) / decimal;
}
