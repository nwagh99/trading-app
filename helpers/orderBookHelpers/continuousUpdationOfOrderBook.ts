import round from './roundUp';
import { continuousUpdateType, OrderBook, Trade } from './customDataType';
import { OrderBookEnum } from './constants';

const find = (
  price: number,
  orderBook: Trade[],
  type: OrderBookEnum
): number => {
  let index = -1;

  for (let i = 0; i < orderBook.length; i++) {
    if (orderBook[i].price === price) {
      return i;
    }
    if (type == OrderBookEnum.bids) {
      if (orderBook[i].price > price) {
        index = i;
      }
    } else if (type === OrderBookEnum.asks) {
      if (orderBook[i].price < price) {
        index = i;
      }
    }
  }
  return index + 1;
};

const reCalculateCummulativeFreq = (
  tradeIndex: number,
  trade: OrderBook,
  orderBook: Trade[]
): Trade[] => {
  const [price, count, amount] = trade;
  const newOrderBook = [...orderBook];
  newOrderBook[tradeIndex] = {
    ...newOrderBook[tradeIndex],
    price: price,
    count: count,
    amount: Math.abs(round(amount)),
    total:
      tradeIndex === 0
        ? Math.abs(round(amount))
        : round(newOrderBook[tradeIndex - 1].total + Math.abs(amount)),
  };
  for (let i = tradeIndex + 1; i < newOrderBook.length; i++) {
    newOrderBook[i] = {
      ...newOrderBook[i],
      total:
        tradeIndex === 0
          ? Math.abs(round(newOrderBook[i].amount))
          : round(newOrderBook[i - 1].total + Math.abs(newOrderBook[i].amount)),
    };
  }

  return newOrderBook;
};

const addTradeNreCalculateCummulativeFreq = (
  tradeIndex: number,
  trade: OrderBook,
  orderBook: Trade[]
): Trade[] => {
  const [price, count, amount] = trade;
  const addTrade = {
    price,
    count,
    amount: Math.abs(round(amount)),
    total: Math.abs(round(amount)),
  };
  const newOrderBook = [...orderBook];
  newOrderBook.splice(tradeIndex, 0, addTrade);
  return reCalculateCummulativeFreq(tradeIndex, trade, newOrderBook);
};

const addUpdateOrderBook = (
  trade: OrderBook,
  orderBook: Trade[],
  type: OrderBookEnum
): Trade[] => {
  const [price, count, amount] = trade;
  console.log(
    type +
      ' order book which is came for update/add ' +
      JSON.stringify(orderBook)
  );

  const tradeIndex = find(price, orderBook, type);
  console.log(price + ' trade found at ', tradeIndex);

  if (tradeIndex < orderBook.length && orderBook[tradeIndex].price === price) {
    return reCalculateCummulativeFreq(tradeIndex, trade, orderBook);
  } else {
    return addTradeNreCalculateCummulativeFreq(tradeIndex, trade, orderBook);
  }
};

const afterDeletion_ReCalculateCummulativeFreq = (
  tradeIndex: number,
  orderBook: Trade[]
): Trade[] => {
  const newOrderBook = [...orderBook];
  for (let i = tradeIndex; i < newOrderBook.length; i++) {
    newOrderBook[i] = {
      ...newOrderBook[i],
      total:
        tradeIndex === 0
          ? round(newOrderBook[i].amount)
          : round(newOrderBook[i - 1].total + newOrderBook[i].amount),
    };
  }
  return newOrderBook;
};

const removeFromOrderBook = (
  trade: OrderBook,
  orderBook: Trade[],
  type: OrderBookEnum
): Trade[] => {
  const [price, count, amount] = trade;
  const tradeIndex = find(price, orderBook, type);
  const newOrderBook = [...orderBook];
  newOrderBook.splice(tradeIndex, 1);
  return afterDeletion_ReCalculateCummulativeFreq(tradeIndex, newOrderBook);
};

export const continuousUpdationOfOrderBook = (
  currentTrade: OrderBook,
  bids: Trade[],
  asks: Trade[]
): continuousUpdateType => {
  const [, count, amount] = currentTrade;

  if (count === 0) {
    // count===0 means we need to remove current ask/bid from respective order book.
    if (amount === 1) {
      // amount===1 means remove currentTrade from bids order book.
      const bidsOrderBook = removeFromOrderBook(
        currentTrade,
        bids,
        OrderBookEnum.bids
      );
      return {
        orderBookData: bidsOrderBook,
        orderBookType: OrderBookEnum.bids,
      };
    } else if (amount === -1) {
      // amount === -1 means remove currentTrade from asks order book
      const asksOrderBook = removeFromOrderBook(
        currentTrade,
        asks,
        OrderBookEnum.asks
      );
      return {
        orderBookData: asksOrderBook,
        orderBookType: OrderBookEnum.asks,
      };
    }
  } else if (count > 0) {
    //count > 0 means, we need to add currentTrade into respective ask/bid order book if not present else update.
    if (amount > 0) {
      // amount > 0 means, we need to add currentTrade into bids order book if not present else update.
      const bidsOrderBook = addUpdateOrderBook(
        currentTrade,
        bids,
        OrderBookEnum.bids
      );
      return {
        orderBookData: bidsOrderBook,
        orderBookType: OrderBookEnum.bids,
      };
    } else if (amount < 0) {
      // amount < 0 means, we need to add currentTrade into asks order book if not present else update.
      const asksOrderBook = addUpdateOrderBook(
        currentTrade,
        asks,
        OrderBookEnum.asks
      );
      return {
        orderBookData: asksOrderBook,
        orderBookType: OrderBookEnum.asks,
      };
    }
  }
  return { orderBookData: [], orderBookType: OrderBookEnum.bids };
};
