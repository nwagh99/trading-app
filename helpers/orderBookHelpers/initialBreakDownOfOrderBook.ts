import round from './roundUp';
import { OrderBook, Trade } from './customDataType';

const createOrderBookData = (
  orderBook: OrderBook[]
): { bids: Trade[]; asks: Trade[] } => {
  const bids: Trade[] = [];
  const asks: Trade[] = [];
  for (let i = 0; i < orderBook.length; i++) {
    let [price, count, amount] = orderBook[i];
    // if amount > 0 then its bid trade else ask
    if (amount > 0) {
      bids.push({ price, count, amount: round(amount), total: round(amount) });
    } else {
      asks.push({
        price,
        count,
        amount: Math.abs(round(amount)),
        total: Math.abs(round(amount)),
      });
    }
  }
  return { bids, asks };
};

const addCumulativeFrequecy = (orderBook: Trade[]): Trade[] => {
  const newOrderBook = [...orderBook];
  newOrderBook[0] = { ...newOrderBook[0], total: orderBook[0].amount };
  for (let i = 1; i < newOrderBook.length; i++) {
    let currentTrade = newOrderBook[i];
    // To calculate total value for current trade, add current trade's amount and previous trade's total
    currentTrade = {
      ...currentTrade,
      total: round(currentTrade.amount + orderBook[i - 1].total),
    };
    newOrderBook[i] = currentTrade;
  }
  return newOrderBook;
};

const initialBreakDownOfOderBook = (orderbook: OrderBook[]) => {
  const { bids, asks } = createOrderBookData(orderbook);

  return {
    bidsOrderBook: addCumulativeFrequecy(bids),
    asksOrderBook: addCumulativeFrequecy(asks),
  };
};

export default initialBreakDownOfOderBook;
