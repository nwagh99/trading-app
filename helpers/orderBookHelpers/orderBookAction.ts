import { OrderBook, Trade } from './customDataType';

export const isOrderBook = (orderBook: OrderBook[] | OrderBook): boolean => {
  const tradeLength = 3;
  return orderBook.length > tradeLength ? true : false;
};

export const findMax = (orderbook: Trade[]): number => {
  let max = 0;
  for (let i = 0; i < orderbook.length; i++) {
    if (max < orderbook[i].amount) {
      max = orderbook[i].amount;
    }
  }
  return max;
};
