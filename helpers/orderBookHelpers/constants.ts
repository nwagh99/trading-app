export enum OrderBookEnum {
    bids = 'bids',
    asks = 'asks'
  }

const BIDS_HEADER = ['COUNT', 'AMOUNT', 'TOTAL', 'PRICE'];

const ASKS_HEADER = ['PRICE', 'TOTAL', 'AMOUNT', 'COUNT'];

export const HEADER_MAPPING = {
  bids :  BIDS_HEADER,
  asks : ASKS_HEADER
}

export const backgroundColor = {
    asks:'red',
    bids:'green'
}