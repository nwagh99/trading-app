import { Dispatch, SetStateAction } from 'react';
import { OrderBookEnum } from './constants';

export type Trade = {
  price: number;
  count: number;
  amount: number;
  total: number;
};

export type ReducerState = {
  bids: Trade[];
  asks: Trade[];
};

export type ReducerAction = {
  type: string;
  payload: Trade[];
};

export type OrderBook = [price: number, count: number, amount: number];

export type continuousUpdateType = {
  orderBookData: Trade[];
  orderBookType: OrderBookEnum;
};
