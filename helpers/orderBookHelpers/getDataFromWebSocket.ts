function getDataFromWebSocket(
  orderBookWebSocketURL: string,
  orderbookSubscription: string,
  callback: Function
) {
  console.log('Web Socket called');

  const webSocket = new WebSocket(orderBookWebSocketURL);

  webSocket.onmessage = (tradeData) => {
    const data = JSON.parse(tradeData.data);
    const currentTrade = data[1]; // data[1] refers to the orderbook:number[][] or trade:number[]
    if (Array.isArray(currentTrade)) {
        callback(currentTrade);
    }
  };

  webSocket.onopen = () => webSocket.send(orderbookSubscription);
}

export default getDataFromWebSocket;
