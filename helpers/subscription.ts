export const candlesSubscription = JSON.stringify({
  event: 'subscribe',
  channel: 'candles',
  key: 'trade:1m:tBTCUSD', //'trade:TIMEFRAME:SYMBOL'
});

export const orderBookSubscription = JSON.stringify({
  event: 'subscribe',
  channel: 'book',
  symbol: 'tBTCUSD',
});
