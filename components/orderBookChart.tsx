import { useEffect, useState } from 'react';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip } from 'recharts';

export default function OrderBookChart(props: {
  type: string;
  chart: object[];
}) {
  const [color, setColor] = useState<string>('');
  useEffect(function () {
    if (props.type === 'buy') {
      setColor('#00cc66');
    } else {
      setColor('#ff3300');
    }
  }, []);
  return (
    <BarChart width={350} height={400} data={props.chart} layout="vertical">
      <CartesianGrid stroke="#ccc" />
      <XAxis hide={true} type="number" />
      <YAxis hide={true} dataKey="price" type="category" />
      {/* <Tooltip /> */}
      <Bar dataKey="total" fill={color} />
    </BarChart>
  );
}
