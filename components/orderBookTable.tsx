import {
  backgroundColor,
  HEADER_MAPPING,
} from '../helpers/orderBookHelpers/constants';
import { Trade } from '../helpers/orderBookHelpers/customDataType';
import { OrderBookEnum } from '../helpers/orderBookHelpers/constants';
import { styles } from '../styles/orderBookCSS';
import { findMax } from '../helpers/orderBookHelpers/orderBookAction';
import { useMemo } from 'react';

export default function OrderBookTable(props: {
  type: OrderBookEnum;
  orderbook: Trade[];
}) {
  const maxAmount = useMemo(() => findMax(props.orderbook), [props.orderbook]);

  return (
    <div className={styles.orderBookTable}>
      <div>
        {
          <ul className={styles.orderBookHeading}>
            {HEADER_MAPPING[props.type].map((title) => (
              <li key={title}>{title}</li>
            ))}
          </ul>
        }
      </div>
      <div>
        {props.orderbook.map((trade: Trade) => {
          const percentage = (trade.total / maxAmount) * 10;
          return (
            <div className={styles.orderBookDataWrapper}>
              <div
                className={styles.wrapperBackGround(
                  backgroundColor[props.type],
                  percentage
                )}
              >
                {' '}
              </div>
              {props.type === OrderBookEnum.bids ? (
                <ul key={trade.price} className={styles.bidsOrderBookData}>
                  <li>{trade.count}</li>
                  <li>{trade.amount}</li>
                  <li>{trade.total}</li>
                  <li>{trade.price}</li>
                </ul>
              ) : (
                <ul key={trade.price} className={styles.asksOrderBookData}>
                  <li>{trade.price}</li>
                  <li>{trade.total}</li>
                  <li>{trade.amount}</li>
                  <li>{trade.count}</li>
                </ul>
              )}
            </div>
          );
        })}
      </div>
    </div>
  );
}
