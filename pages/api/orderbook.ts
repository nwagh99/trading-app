import axios from 'axios';

export default async function handler(req: any, res: any) {
  try {
    let response = await axios.get(
      'https://api.bitfinex.com/v2/book/tBTCUSD/P0'
    );
    if (response) {
      return res.status(200).send(response.data);
    }
  } catch (err) {
    return res.status(400).send(err);
  }
}
