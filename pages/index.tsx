import styles from '../styles/Home.module.css';
import OhlcChart from '../../ohlc';
import ChartComponent from '../../candleStick';
import LiveOrderBook from './orderBook';
import LiveOHLC from '../../liveohlcChart';

export default function Home() {
  return (
    <>
      {/* <OhlcChart/> */}
      {/* <ChartComponent/> */}
      {/* <LiveOHLC/> */}

      <LiveOrderBook />
    </>
  );
}
