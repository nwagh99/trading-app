import OrderBookTable from '../components/orderBookTable';
import { useEffect, useState, useRef } from 'react';
import { continuousUpdationOfOrderBook } from '../helpers/orderBookHelpers/continuousUpdationOfOrderBook';
import { OrderBook, Trade } from '../helpers/orderBookHelpers/customDataType';
import getDataFromWebSocket from '../helpers/orderBookHelpers/getDataFromWebSocket';
import initialBreakDownOfOderBook from '../helpers/orderBookHelpers/initialBreakDownOfOrderBook';
import { isOrderBook } from '../helpers/orderBookHelpers/orderBookAction';
import { orderBookSubscription } from '../helpers/subscription';
import orderBookWebSocketURL from '../helpers/webSocketURL';
import { OrderBookEnum } from '../helpers/orderBookHelpers/constants';
import { styles } from '../styles/orderBookCSS';

function LiveOrderBook() {
  const bids = useRef<Trade[]>([]);
  const asks = useRef<Trade[]>([]);

  const [bidsState, setBidsState] = useState<Trade[]>([]);
  const [asksState, setAsksState] = useState<Trade[]>([]);

  useEffect(() => {
    const intervalId = setInterval(() => {
      setBidsState(bids.current);
      setAsksState(asks.current);
    }, 1000);

    return () => {
      clearInterval(intervalId);
    };
  }, []);

  useEffect(() => {
    const callback = (currentTrade: OrderBook[] | OrderBook): void => {
      console.log('Current Trade ', currentTrade);

      if (isOrderBook(currentTrade)) {
        const { bidsOrderBook, asksOrderBook } =
          initialBreakDownOfOderBook(currentTrade);

        bids.current = [...bidsOrderBook];
        asks.current = [...asksOrderBook];
      } else {
        const { orderBookData, orderBookType } = continuousUpdationOfOrderBook(
          currentTrade,
          bids.current,
          asks.current
        );

        if (orderBookType === OrderBookEnum.bids) {
          bids.current = [...orderBookData];
        } else {
          asks.current = [...orderBookData];
        }
      }
    };

    getDataFromWebSocket(
      orderBookWebSocketURL,
      orderBookSubscription,
      callback
    );
  }, []);

  return (
    <>
      <div className={styles.orderBookCompo}>
        <OrderBookTable type={OrderBookEnum.bids} orderbook={bidsState} />
        <OrderBookTable type={OrderBookEnum.asks} orderbook={asksState} />
      </div>
    </>
  );
}

export default LiveOrderBook;
