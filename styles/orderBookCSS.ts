import { css } from '@emotion/css';
import { strict } from 'assert';
import {
  backgroundColor,
  OrderBookEnum,
} from '../helpers/orderBookHelpers/constants';

export const styles = {
  orderBookCompo: css`
    display: flex;
    width: 900px;
    margin: auto;
    height: 580px;
  `,

  orderBookTable: css`
      color: rgb(255, 255, 255);
      background: rgb(16, 35, 49);
      // line-height: 1.5 ;
      font-family: Roboto, sans-serif;
      font-weight: medium;
      cursor: pointer;
      font-size: 8px
      width:450px,
  `,
  orderBookHeading: css`
    display: flex;
    justify-content: space-around;
    list-style: none;
    margin-bottom: 10px;
  `,
  orderBookDataWrapper: css`
    position: relative;
    width: 450px;
    z-index: 2;
  `,
  bidsOrderBookData: css`
    margin-left: 25px;
    right: 0;
    list-style: none;
    display: flex;
    justify-content: space-around;
    z-index: 2;
    height: 5px;
  `,
  asksOrderBookData: css`
    margin-right: 50px;
    left: 0%;
    list-style: none;
    display: flex;
    justify-content: space-around;
    z-index: 2;
    height: 5px;
  `,
  wrapperBackGround: (color: string, percentage: number) => css`
    position: absolute;
    opacity: 0.3;
    z-index: -2;
    background: ${color};
    width: ${percentage}%;
    max-width: 100%;
    top: 0;
    bottom: 0;
    ${color === backgroundColor.bids ? `right:0;` : `left:0 ;`}
    right:0;
    height: 21px;
  `,
};
